//function SimpleFunction.test 2
(SimpleFunction.test)
//function SimpleFunction.test 2
@1
A = M
D = M
@SP
A = M
M = D
@0
M = M + 1
//function SimpleFunction.test 2
@1
A = M
A=A+1
D = M
@SP
A = M
M = D
@0
M = M + 1
//push local 0
@1
A = M
D = M
@SP
A = M
M = D
@0
M = M + 1
//push local 1
@1
A = M
A=A+1
D = M
@SP
A = M
M = D
@0
M = M + 1
//add
@SP
A = M
A = A -1
D = M
@SP
A = M
A = A -1
A = A -1
D = D + M
@SP
A = M
A = A -1
A = A -1
M = D
@0
M = M - 1
//not
@SP
A = M
A = A -1
D = M
@SP
A = M
A = A -1
M=!D
//push argument 0
@2
A = M
D = M
@SP
A = M
M = D
@0
M = M + 1
//add
@SP
A = M
A = A -1
D = M
@SP
A = M
A = A -1
A = A -1
D = D + M
@SP
A = M
A = A -1
A = A -1
M = D
@0
M = M - 1
//push argument 1
@2
A = M
A=A+1
D = M
@SP
A = M
M = D
@0
M = M + 1
//sub
@SP
A = M
A = A -1
D = M
@SP
A = M
A = A -1
A = A -1
D=M-D
@SP
A = M
A = A -1
A = A -1
M = D
@0
M = M - 1
@SP
A = M
A = A -1
D = M
@7
M = D
// Frame = LC
@1
D = M
@5
M = D
//*(Frame -5
@5
A = M
A = A -1
A = A -1
A = A -1
A = A -1
A = A -1
D = M
@6
M = D
//*ARG = pop()
@R2
D = M + 1
@R0
M = D
//THAT = *(Frame -1)
@5
A = M
A = A -1
D = M
@R4
M = D
//THIS = *(Frame -2)
@5
A = M
A = A -1
A = A -1
D = M
@R3
M = D
//ARG = *(Frame -3)
@5
A = M
A = A -1
A = A -1
A = A -1
D = M
@R2
M = D
//ARG = *(Frame -4)
@5
A = M
A = A -1
A = A -1
A = A -1
A = A -1
D = M
@R1
M = D
//goto RET
@6
A = M
@7
D = M
@SP
A = M -1
M = D
0;JEQ
