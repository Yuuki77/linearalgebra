@256
D = A
@0
M = D
@300
D = A
@1
M = D
@400
D = A
@2
M = D
@3000
D = A
@3
M = D
@3010
D = A
@4
M = D
//push constant 0
@0
D = A
@SP
A = M
M = D
@0
M = M + 1
//pop local 0         
@SP
A = M
A = A -1
D = M
@1
A = M
M = D
@0
M = M - 1
//label LOOP_START
(LOOP_START)
//push argument 0
@2
A = M
D = M
@SP
A = M
M = D
@0
M = M + 1
//push local 0
@1
A = M
D = M
@SP
A = M
M = D
@0
M = M + 1
//add
@SP
A = M
A = A -1
D = M
@SP
A = M
A = A -1
A = A -1
D = D + M
@SP
A = M
A = A -1
A = A -1
M = D
@0
M = M - 1
//pop local 0	        
@SP
A = M
A = A -1
D = M
@1
A = M
M = D
@0
M = M - 1
//push argument 0
@2
A = M
D = M
@SP
A = M
M = D
@0
M = M + 1
//push constant 1
@1
D = A
@SP
A = M
M = D
@0
M = M + 1
//sub
@SP
A = M
A = A -1
D = M
@SP
A = M
A = A -1
A = A -1
D=M-D
@SP
A = M
A = A -1
A = A -1
M = D
@0
M = M - 1
//pop argument 0      
@SP
A = M
A = A -1
D = M
@2
A = M
M = D
@0
M = M - 1
//push argument 0
@2
A = M
D = M
@SP
A = M
M = D
@0
M = M + 1
//if-goto LOOP_START  
@0
M = M - 1
@SP
A = M
D = M
@LOOP_START
D;JNE
//push local 0
@1
A = M
D = M
@SP
A = M
M = D
@0
M = M + 1
