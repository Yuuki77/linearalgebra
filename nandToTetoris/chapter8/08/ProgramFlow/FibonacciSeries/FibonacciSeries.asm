@256
D = A
@0
M = D
@300
D = A
@1
M = D
@400
D = A
@2
M = D
@3000
D = A
@3
M = D
@3010
D = A
@4
M = D
//push argument 1
@2
A = M
A=A+1
D = M
@SP
A = M
M = D
@0
M = M + 1
//pop pointer 1           
@SP
A = M
A = A -1
D = M
@3
A=A+1
M = D
@0
M = M - 1
//push constant 0
@0
D = A
@SP
A = M
M = D
@0
M = M + 1
//pop that 0              
@SP
A = M
A = A -1
D = M
@4
A = M
M = D
@0
M = M - 1
//push constant 1
@1
D = A
@SP
A = M
M = D
@0
M = M + 1
//pop that 1              
@SP
A = M
A = A -1
D = M
@4
A = M
A=A+1
M = D
@0
M = M - 1
//push argument 0
@2
A = M
D = M
@SP
A = M
M = D
@0
M = M + 1
//push constant 2
@2
D = A
@SP
A = M
M = D
@0
M = M + 1
//sub
@SP
A = M
A = A -1
D = M
@SP
A = M
A = A -1
A = A -1
D=M-D
@SP
A = M
A = A -1
A = A -1
M = D
@0
M = M - 1
//pop argument 0          
@SP
A = M
A = A -1
D = M
@2
A = M
M = D
@0
M = M - 1
//label MAIN_LOOP_START
(MAIN_LOOP_START)
//push argument 0
@2
A = M
D = M
@SP
A = M
M = D
@0
M = M + 1
//if-goto COMPUTE_ELEMENT 
@0
M = M - 1
@SP
A = M
D = M
@COMPUTE_ELEMENT
D;JNE
//goto END_PROGRAM        
@0
M = M - 1
@SP
A = M
D = M
@END_PROGRAM
0;JEQ
//label COMPUTE_ELEMENT
(COMPUTE_ELEMENT)
//push that 0
@4
A = M
D = M
@SP
A = M
M = D
@0
M = M + 1
//push that 1
@4
A = M
A=A+1
D = M
@SP
A = M
M = D
@0
M = M + 1
//add
@SP
A = M
A = A -1
D = M
@SP
A = M
A = A -1
A = A -1
D = D + M
@SP
A = M
A = A -1
A = A -1
M = D
@0
M = M - 1
//pop that 2              
@SP
A = M
A = A -1
D = M
@4
A = M
A=A+1
A=A+1
M = D
@0
M = M - 1
//push pointer 1
@3
A=A+1
D = M
@SP
A = M
M = D
@0
M = M + 1
//push constant 1
@1
D = A
@SP
A = M
M = D
@0
M = M + 1
//add
@SP
A = M
A = A -1
D = M
@SP
A = M
A = A -1
A = A -1
D = D + M
@SP
A = M
A = A -1
A = A -1
M = D
@0
M = M - 1
//pop pointer 1           
@SP
A = M
A = A -1
D = M
@3
A=A+1
M = D
@0
M = M - 1
//push argument 0
@2
A = M
D = M
@SP
A = M
M = D
@0
M = M + 1
//push constant 1
@1
D = A
@SP
A = M
M = D
@0
M = M + 1
//sub
@SP
A = M
A = A -1
D = M
@SP
A = M
A = A -1
A = A -1
D=M-D
@SP
A = M
A = A -1
A = A -1
M = D
@0
M = M - 1
//pop argument 0          
@SP
A = M
A = A -1
D = M
@2
A = M
M = D
@0
M = M - 1
//goto MAIN_LOOP_START
@0
M = M - 1
@SP
A = M
D = M
@MAIN_LOOP_START
0;JEQ
//label END_PROGRAM
(END_PROGRAM)
