using System;
using System.Collections.Generic;
using System.IO;

namespace Chapter7
{
	public class CodeWriter
	{
		private string setFileName;
		private List<string> results = new List<string>();
		private string outputPath;
		private string fileName;

		private Dictionary<string, int> baseAddresses = new Dictionary<string, int>() {
			{"sp", 0},
			{"local", 1},
			{"argument",2},
			{ "this", 3},
			{ "that",4},
			{ "temp",5},
			{ "pointer", 3},
			{ "static" ,16}
		};

		private Dictionary<string, string> operations = new Dictionary<string, string>()
		{
			{"sub","D=M-D"},
			{"add","D = D + M"},
			{"and","D=D&M"},
			{"or", "D=D|M"},

			{"eq","D;JEQ"},
			{"neg","M=-D"},
			{"gt", "D;JLT"},
			{"lt","D;JGT"},
			{"not","M=!D"},
			{"if-goto","D;JNE"},
			{"goto","0;JEQ"}
		};

		public void WriteFunction(string functionName, int numArgs)
		{
			results.Add("//" + Chapter7.Parser.debugcurrentCommand);
			results.Add($"({functionName})");
			for (var i = 0; i < numArgs; i++)
			{
				WritePushPop(Chapter7.CommandType.C_PUSH, "local", i);
			}
		}

		public void WriteReturn()
		{
			// Frame = LCL
			// results.Add("@0");
			// results.Add("D = M");
			WriteSpToARegister(1);
			results.Add("D = M");
			results.Add("@7");
			results.Add("M = D");

			results.Add("// Frame = LC");

			results.Add("@1");
			results.Add("D = M");
			results.Add("@5");
			results.Add("M = D");

			// *(Frame -5)
			results.Add("//*(Frame -5");
			results.Add("@5");
			results.Add("A = M");

			for(var i = 0; i < 5; i++) {
				results.Add("A = A -1");
			}
			results.Add("D = M");
			results.Add("@6");
			results.Add("M = D");

			results.Add("//*ARG = pop()");

			results.Add("@R2");
			results.Add("D = M + 1");
			results.Add("@R0");
			results.Add("M = D");

			// That
			results.Add("//THAT = *(Frame -1)");
			results.Add("@5");
			results.Add("A = M");
			results.Add("A = A -1");
			results.Add("D = M");
			results.Add("@R4");
			results.Add("M = D");

			// This
			results.Add("//THIS = *(Frame -2)");
			results.Add("@5");
			results.Add("A = M");
			results.Add("A = A -1");
			results.Add("A = A -1");
			results.Add("D = M");
			results.Add("@R3");
			results.Add("M = D");

			// ARG
			results.Add("//ARG = *(Frame -3)");
			results.Add("@5");
			results.Add("A = M");
			results.Add("A = A -1");
			results.Add("A = A -1");
			results.Add("A = A -1");

			results.Add("D = M");
			results.Add("@R2");
			results.Add("M = D");

			results.Add("//ARG = *(Frame -4)");
			results.Add("@5");
			results.Add("A = M");
			results.Add("A = A -1");
			results.Add("A = A -1");
			results.Add("A = A -1");
			results.Add("A = A -1");

			results.Add("D = M");
			results.Add("@R1");
			results.Add("M = D");

			results.Add("//goto RET");
			results.Add("@6");
			results.Add("A = M");

			results.Add("@7");
			results.Add("D = M");
			results.Add("@SP");
			results.Add("A = M -1");
			results.Add("M = D");

			results.Add("0;JEQ");

		// results.Add(operations[add_or_sub_key]);

		}

		private int symbolIndex = 0;

		private string trueSymbolString
		{
			get
			{
				return $"Success_{symbolIndex}";
			}
		}

		private string endSymbolString
		{
			get
			{
				return $"end_{symbolIndex}";
			}
		}

		public CodeWriter(string output, string fileName)
		{
			this.outputPath = output;
			this.fileName = fileName;

			// UpdateBaseAddress("sp", 256);
			// UpdateBaseAddress("local", 300);
			// UpdateBaseAddress("argument", 400);
			// UpdateBaseAddress("this", 3000);
			// UpdateBaseAddress("that", 3010);
		}

		public void WriteAthematic(string command, string operation)
		{
			results.Add("//" + Chapter7.Parser.debugcurrentCommand);

			switch (command)
			{
				case "add":
				case "sub":
				case "and":
				case "or":
					ExecuteAddSubOperation(command);
					break;
				case "gt":
				case "lt":
				case "eq":
					ExecuteEqOrGtOrLtOperation(command);
					break;
				case "neg":
				case "not":
					ExecuteNegOrNotOperation(command);
					break;
				default:
					throw new Exception("Unexpected " + operation);
			}
			// WriteResultStackSp();
		}

		private void ExecuteAddSubOperation(string add_or_sub_key)
		{
			WriteSpToARegister(1);
			results.Add("D = M");
			WriteSpToARegister(2);

			results.Add(operations[add_or_sub_key]);
			WriteSpToARegister(2);
			results.Add("M = D");
			UpdateStackSPAddress(false);
		}

		public void WriteIf(string operation, CommandType commandType)
		{
			results.Add("//" + Chapter7.Parser.debugcurrentCommand);

			UpdateStackSPAddress(false);
			WriteSpToARegister(0);
			results.Add("D = M");
			results.Add($"@{operation}");

			string asm_operation = commandType == CommandType.C_IF ? "if-goto" : "goto";
			results.Add(operations[asm_operation]);
		}

		public void WriteLabel(string operation)
		{
			results.Add("//" + Chapter7.Parser.debugcurrentCommand);
			results.Add("(" + operation + ")");
		}

		private void ExecuteNegOrNotOperation(string cmd)
		{
			WriteSpToARegister(1);
			results.Add("D = M");
			WriteSpToARegister(1);
			results.Add(operations[cmd]);
		}

		private void ExecuteEqOrGtOrLtOperation(string command)
		{
			WriteSpToARegister(1);
			results.Add("D = M");
			WriteSpToARegister(2);

			results.Add("D = D - M");

			results.Add($"@{trueSymbolString}");
			results.Add(operations[command]);

			// else の処理を書く
			WriteSpToARegister(2);
			results.Add("M = 0");
			results.Add($"@{endSymbolString}");
			results.Add("0;JEQ");
			results.Add($"({trueSymbolString})");
			WriteSpToARegister(2);
			results.Add("M = -1");
			results.Add($"({endSymbolString})");
			UpdateStackSPAddress(false);
			symbolIndex++;
		}

		public void WritePushPop(Chapter7.CommandType commandType, string segment, int index)
		{
			results.Add("//" + Chapter7.Parser.debugcurrentCommand);

			if (segment == "constant")
			{
				PushPopConstant(commandType, index);
			}
			else
			{
				if (commandType == Chapter7.CommandType.C_PUSH)
				{
					WriteMemorySegment(segment, index);
					results.Add("D = M");
					PushToStack();
				}
				else
				{
					WriteSpToARegister(1);
					results.Add("D = M");

					WriteMemorySegment(segment, index);
					results.Add("M = D");
					UpdateStackSPAddress(false);
				}
			}
		}

		private void WriteMemorySegment(string segment, int index)
		{
			results.Add("@" + baseAddresses[segment]);
			if (segment != "temp" && segment != "pointer" && segment != "static")
			{
				results.Add("A = M");
			}

			for (var i = 0; i < index; i++)
			{
				results.Add("A=A+1");
			}
		}

		public void WriteResult()
		{
			string path = Path.GetDirectoryName(this.setFileName);
			string fileName = Path.GetFileNameWithoutExtension(this.setFileName);
			File.WriteAllLines(outputPath, results);
		}

		private void WriteSpToARegister(int diff)
		{
			results.Add("@SP");
			results.Add("A = M");

			for(var i = 0; i < diff; i++) {
				results.Add("A = A -1");
			}
		}

		public void UpdateBaseAddress(string key, int index) {
			results.Add("@" + index);
			results.Add("D = A");
			results.Add($"@{baseAddresses[key]}");
			results.Add("M = D");
		}

		public void UpdateStackSPAddress(bool up) {
			results.Add($"@{0}");
			if(up) {
				results.Add("M = M + 1");

			} else {
				results.Add("M = M - 1");
			}

		}

		private void PushPopConstant(CommandType commandType, int index)
		{
			if (commandType == CommandType.C_PUSH)
			{
				results.Add("@" + index);
				results.Add("D = A");
				PushToStack();
			}
			else
			{
				throw new Exception("Un expected pop stack option");
			}
		}

		private void PushToStack() {
				results.Add("@SP");
				results.Add("A = M");
				results.Add("M = D");
				UpdateStackSPAddress(true);
		}
	}
}